data "google_dns_managed_zone" "pit-parent" {
  name = "gcp-cwxlab-fr"
}

resource "google_dns_managed_zone" "pit" {
  name        = "pit-gcp-cwxlab-fr"
  dns_name    = "pit.gcp.cwxlab.fr."
  description = "Zone DNS du Projet IT"
}

resource "google_dns_record_set" "deleguation" {
  name = "${google_dns_managed_zone.pit.dns_name}"
  type = "NS"
  ttl  = 300

  rrdatas = google_dns_managed_zone.pit.name_servers

  managed_zone = data.google_dns_managed_zone.pit-parent.name
}
