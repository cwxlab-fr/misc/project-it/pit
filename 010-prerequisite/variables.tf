variable "project" {
  description = "Le project par défault dans lequel gerer les resources"
  type        = string

  validation {
    condition     = contains(["cwxlab-415221"], var.project)
    error_message = "Les projets autorisés sont: cwxlab-415221"
  }
}

variable "region" {
  description = "La region par default dans laquelle gérer les resources"
  type        = string

  validation {
    condition     = contains(["europe-west1"], var.region)
    error_message = "Les regions autorisées sont: europe-west1"
  }
}

variable "zone" {
  description = "La zone par default dans laquelle gérer les resources"
  type        = string

  validation {
    condition     = contains(["europe-west1-b", "europe-west1-c", "europe-west1-d"], var.zone)
    error_message = "Les zones autorisées sont: europe-west1-b, europe-west1-c, europe-west1-d"
  }
}

variable "deployer_impersonators" {
  description = "Identities that can impersonate the deployer service account"
  type        = list(string)
}
