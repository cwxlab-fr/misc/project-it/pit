resource "google_project_iam_custom_role" "pit_deployer" {
  role_id     = "pitDeployer"
  title       = "Project IT deployer"
  description = "Role defining required permission to deploy Project IT"

  permissions = [
    "compute.networks.get",
    "compute.networks.list",
    "compute.networks.create",
  ]
}


resource "google_service_account" "pit_deployer" {
  account_id   = "pit-deployer"
  display_name = "Project IT deployer"
  description  = "Service account for deploying Project IT"
}

resource "google_project_iam_member" "project" {
  project = var.project
  role    = google_project_iam_custom_role.pit_deployer.id
  member  = google_service_account.pit_deployer.member
}

resource "google_service_account_iam_binding" "project" {
  service_account_id = google_service_account.pit_deployer.name
  role               = "roles/iam.serviceAccountUser"

  members = var.deployer_impersonators
}

resource "time_rotating" "pit_deployer_key_rotation" {
  rotation_days = 1
}

resource "google_service_account_key" "pit_deployer_key" {
  service_account_id = google_service_account.pit_deployer.name
  public_key_type    = "TYPE_X509_PEM_FILE"

  keepers = {
    rotation_time = time_rotating.pit_deployer_key_rotation.rotation_rfc3339
  }
}

resource "local_sensitive_file" "pit_deployer_private_key" {
  content_base64  = google_service_account_key.pit_deployer_key.private_key
  filename        = "${path.module}/../pit_deployer_key.json"
  file_permission = "0640"
}
